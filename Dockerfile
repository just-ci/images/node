ARG IMAGE_TAG=alpine
FROM node:${IMAGE_TAG}

RUN apk add --no-cache git jq git-lfs curl dasel

RUN npm install -g npm@latest && \
    npm install -g semantic-release@latest @semantic-release/gitlab@latest @semantic-release/git@latest @semantic-release/changelog@latest @semantic-release/exec@latest && \
    npm cache clear --force

ENTRYPOINT ["/bin/sh", "-c"]
